import React, { Component } from 'react';

class Button extends Component {
    //props or property
    render() {
        return (
            <button
                className={this.props.color}
                //4. use the function we received as property in our event listener property(onClick)
                onClick={this.props.handleOnClick}

            >{this.props.text}</button>
        );
    }
}

export default Button;