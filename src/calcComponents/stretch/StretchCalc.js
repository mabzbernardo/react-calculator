import React, { useState } from 'react';
import StretchButton from './StretchButton';

const StretchCalc = () => {

    const [input, setInput] = useState("");
    const [previousNum, setPreviousNum] = useState("");
    let [currentNum, setCurrentNum] = useState("");
    const [operator, setOperator] = useState("");
    const [operatorText, setOperatorText] = useState("");


    const addToInput = val => {
        setInput(input + val);
    }

    const addDecimal = val => {
        if (input.indexOf(".") === -1) {
            setInput(input + val);
        }
    }

    const addZero = val => {
        if (input != "") {
            setInput(input + val)
        }
    }

    const clear = () => {
        setInput("");
        setOperatorText("");
        setPreviousNum("")
    }

    const add = (val) => {
        setPreviousNum(input);
        setInput("")
        setOperator("+");
        setOperatorText(val)
    }

    const divide = (val) => {
        setPreviousNum(input);
        setInput("")
        setOperator("/");
        setOperatorText(val)
    }

    const subtract = (val) => {
        setPreviousNum(input);
        setInput("")
        setOperator("-");
        setOperatorText(val)
    }

    const multiply = (val) => {
        setPreviousNum(input);
        setInput("")
        setOperator("x");
        setOperatorText(val)
    }

    // Equals

    const equals = () => {
        currentNum = input;

        if (operator === "+") {
            let result = parseFloat(currentNum) + parseFloat(previousNum);
            let res = result.toString();
            setInput(res);
            setOperatorText("");
            setPreviousNum("")
        } else if (operator === "-") {
            let result = parseFloat(currentNum) - parseFloat(previousNum);
            let res = result.toString();
            setInput(res);
            setOperatorText("");
            setPreviousNum("")
        } else if (operator === "x") {
            let result = parseFloat(currentNum) * parseFloat(previousNum);
            let res = result.toString();
            setInput(res);
            setOperatorText("");
            setPreviousNum("")
        } else if (operator === "/") {
            let result = parseFloat(currentNum) / parseFloat(previousNum);
            let res = result.toString();
            setInput(res);
            setOperatorText("");
            setPreviousNum("")
        }
    }

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-log-12">
                        <div className="row">
                            <span className='text-black p-3'>
                                {previousNum === "" ? "" : previousNum}
                                {' '}
                                {operatorText === "" ? "" : operatorText}
                                {' '}
                                {input === "" ? "" : input}
                            </span>
                        </div>
                        <div className="row">
                            <StretchButton text={'7'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'8'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'9'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'/'} color={'#18e89c'} onClick={divide} />
                        </div>

                        <div className="row">
                            <StretchButton text={'4'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'5'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'6'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'x'} color={'#18e89c'} onClick={multiply} />
                        </div>

                        <div className="row">
                            <StretchButton text={'1'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'2'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'3'} color={'#18e81f'} onClick={addToInput} />
                            <StretchButton text={'+'} color={'#18e89c'} onClick={add} />
                        </div>

                        <div className="row">
                            <StretchButton text={'.'} color={'#18e81f'} onClick={addDecimal} />
                            <StretchButton text={'0'} color={'#18e81f'} onClick={addZero} />
                            <StretchButton text={'='} color={'#18e89c'} onClick={equals} />
                            <StretchButton text={'-'} color={'#18e89c'} onClick={subtract} />
                        </div>

                        <div className="row">
                            <button
                                className="btn btn-danger btn-sm"
                                onClick={clear}
                            >clear</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default StretchCalc;