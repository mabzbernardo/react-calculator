import React from 'react';

const StretchButton = props => {
    return (
        <>
            <button
                className='btn btn-info'
                style={{
                    width: "64px",
                    height: "64px",
                    backgroundColor: props.color
                }}
                onClick={() => { props.onClick(props.text) }}
            >
                <span className="text-black">
                    {props.text}
                </span>
            </button>
        </>
    )
}

export default StretchButton;