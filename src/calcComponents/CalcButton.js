import React from 'react';

const CalcButton = (props) => {
    return (
        <button
            className={props.color}
            onClick={props.handleOnClick}
        >{props.text}</button>
    )
}

export default CalcButton;