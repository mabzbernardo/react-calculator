import React from 'react';
import CalcButton from './CalcButton';

const CalcButtons = (props) => {
    return (
        <div
            className="d-flex justify-content-around"
        >
            <CalcButton
                // handleOnclick={setAdd(handleAdd)}
                text={'+'}
                color={'btn btn-info'}
                handleOnClick={props.handleAdd}
            />

            <CalcButton
                text={'-'}
                color={'btn btn-dark'}
                handleOnClick={props.handleMinus}
            />

            <CalcButton
                text={'*'}
                color={'btn btn-danger'}
                handleOnClick={props.handleMultiply}
            />

            <CalcButton
                text={'/'}
                color={'btn btn-secondary'}
                handleOnClick={props.handleDivide}
            />
        </div>
    )
}

export default CalcButtons;