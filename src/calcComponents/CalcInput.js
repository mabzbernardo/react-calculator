import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

const CalcInput = (props) => {
    return (
        <div className='d-flex justify-content-around'>
            <FormGroup>
                <Label>{props.label}</Label>
                <Input
                    placeholder={props.placeholder}
                    type='number'
                    onChange={props.onChange}
                />
            </FormGroup>
        </div>
    )
}

export default CalcInput;