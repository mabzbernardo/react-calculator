import React, { useState } from 'react';
import { Dropdown, DropdownToggle, FormGroup, Label, DropdownMenu, DropdownItem } from 'reactstrap';
import Currencies from './ForexData'

function ForexDropdown(props) {

    //useState allows us to define method and state. React hook
    //const[state, method]
    const [dropdownIsOpen, setDropdownIsOpen] = useState(false);
    const [currency, setCurrency] = useState(null);

    // console.log(Currencies)
    return (
        <FormGroup>
            <Label>{props.label}</Label>
            <Dropdown
                isOpen={dropdownIsOpen}
                toggle={() => setDropdownIsOpen(!dropdownIsOpen)}
            >
                <DropdownToggle caret>{!props.currency
                    ? "Choose Currency"
                    : props.currency.currency}</DropdownToggle>
                <DropdownMenu>
                    {Currencies.map((currency, index) => (
                        <DropdownItem
                            key={index}
                            onClick={() => props.onClick(currency)}
                        >
                            {currency.currency}
                        </DropdownItem>
                    ))}
                </DropdownMenu>
            </Dropdown>
        </FormGroup>

    );
}

export default ForexDropdown;