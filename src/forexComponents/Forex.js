import React, { useState } from 'react';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button } from 'reactstrap';
import ForexTable from './ForexTable';


//ES6 form
const Forex = () => {

    // state = {
    //     amount: 0,
    //     baseCurrency: null,
    //     targetCurrency: null,
    //     convertedAmount: 0,
    //     messageError: '',
    //     targetCode: null,
    //     displayRates: null
    // }

    const [amount, setAmount] = useState(0);
    const [baseCurrency, setBaseCurrency] = useState(null);
    const [targetCurrency, setTargentCurrency] = useState(null);
    const [convertedAmount, setConvertedAmount] = useState(0);
    const [messageError, setMessageError] = useState('');
    const [targetCode, setTargetCode] = useState(null);
    const [showTable, setShowTable] = useState(false);
    const [rates, setRates] = useState([]);

    const handleAmount = e => {
        // this.setState({
        //     amount: e.target.value
        // })
        setAmount(e.target.value)
    }

    const handleBaseCurrency = currency => {
        // this.setState({
        //     baseCurrency: currency
        // })
        //fetch data
        const code = currency.code;

        fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res => {
                // console.log(res.rates);
                //code to transform array
                const ratesArray = Object.entries(res.rates);

                setRates(ratesArray);
                // console.log(ratesArray)
                setShowTable(true);
            })
        //end fetch data

        setShowTable(true);
        setBaseCurrency(currency)
    }

    const handleTargetCurrency = currency => {
        // this.setState({
        //     targetCurrency: currency
        // })

        setTargentCurrency(currency)
    }

    const handleConvert = () => {
        // console.log('hello')
        //Activity A - Validation
        if (targetCurrency != null && baseCurrency != null && amount > 0) {
            const code = baseCurrency.code;
            // console.log('hello')
            setMessageError("")
            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
                .then(res => res.json())
                .then(res => {
                    // console.log(res) output rates
                    const targetCode = targetCurrency.code;

                    const rate = res.rates[targetCode];

                    // console.log(rate)

                    // this.setState({ convertedAmount: this.state.amount * rate })
                    setConvertedAmount(amount * rate);

                    // this.setState({ targetCode: targetCode })
                    setTargetCode(targetCode);

                })
        } else {
            setMessageError("Please fill up with valid data")
        }


    }

    // console.log(this.state.amount)
    return (
        <div
            style={{ width: "70%" }}
        >
            <h1 className='text-center my-5'>Forex Calculator</h1>
            {
                showTable === true
                    ?
                    <div>
                        <h2 className='text-center'>Exchange rate for: {baseCurrency.currency}</h2>
                        <div className='d-flex justify-content-center'>
                            <ForexTable
                                rates={rates}
                            />
                        </div>
                    </div>
                    : ""
            }
            <div className='d-flex justify-content-around'
            >
                <ForexDropdown
                    label={'Base Currency'}
                    onClick={handleBaseCurrency}
                    currency={baseCurrency}
                />
                <ForexDropdown
                    label={'Target Currency'}
                    onClick={handleTargetCurrency}
                    currency={targetCurrency}
                />
            </div>
            <div className="d-flex justify-content-around">
                <ForexInput
                    label={'Amount'}
                    placeholder={'Amount to convert'}
                    onChange={handleAmount}
                />
                <Button
                    color='info'
                    onClick={handleConvert}
                // handleBaseCurrency={handleBaseCurrency}
                >Convert
                </Button>
            </div>
            <div>
                <h1 className='text-center'>{convertedAmount}{""}{targetCode}</h1>
                <h5 className='text-center'>{messageError}</h5>
            </div>

        </div>
    );
}

export default Forex;