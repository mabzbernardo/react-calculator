import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
//Counter App
// import App from "./App";
import * as serviceWorker from "./serviceWorker";

//bootstrap
import "bootstrap/dist/css/bootstrap.css";

//Forex App
// import ForexApp from './ForexApp'

// Calc App
import CalcApp from './CalcApp';

//Stretch Calc
import StretchCalc from './calcComponents/stretch/StretchCalc';

ReactDOM.render(
  <React.StrictMode>
    <div className="bg-light">
      <StretchCalc />
    </div>

  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
