import React, { Component } from "react";
import Box from './components/Box';
import Buttons from './components/Buttons'

class App extends Component {
  state = {
    count: 0
  };
  //1. Create function
  handleAdd = () => {
    // console.log('clicked Add')
    //5. set up based on role of buttion
    this.setState({
      count: this.state.count + 1
    })
  }

  //Activity, set up the function of the remaining buttons
  //
  handleMinus = () => {
    // console.log('clicked minus')
    this.setState({
      count: this.state.count - 1
    })
  }

  handleMultiply = () => {
    this.setState({
      count: this.state.count * 2
    })
  }

  handleReset = () => {
    this.setState({
      count: 0
    })
  }

  render() {
    return (
      <>
        //used to be the box
        <Box count={this.state.count} />
        <Buttons
          //2. pass function as propoerty 
          handleAdd={this.handleAdd}
          handleMinus={this.handleMinus}
          handleMultiply={this.handleMultiply}
          handleReset={this.handleReset}
        />
      </>
    );
  }
}

export default App;
