import React, { useState } from 'react';
import CalcInput from './calcComponents/CalcInput';
import CalcButtons from './calcComponents/CalcButtons';
import { Button } from 'reactstrap';

const CalcApp = () => {

    const [inputA, setInputA] = useState(0);
    const [inputB, setInputB] = useState(0);
    let [result, setResult] = useState(0);


    const handleInputA = (e) => {
        setInputA(e.target.value)
        console.log(inputA)
    }

    const handleInputB = (e) => {
        setInputB(e.target.value)
        console.log(inputB)
    }

    const handleAdd = () => {
        setResult(result = parseInt(inputA) + parseInt(inputB));
    }

    const handleMinus = () => {
        setResult(result = parseInt(inputA) - parseInt(inputB));
    }

    const handleMultiply = () => {
        setResult(result = parseInt(inputA) * parseInt(inputB));
    }

    const handleDivide = () => {
        setResult(result = parseInt(inputA) / parseInt(inputB));
    }

    const handleReset = () => {
        setResult(result = 0)
    }

    return (
        <>
            <h1 className="text-center m-5">Calculator App</h1>
            <CalcInput
                label={'Number A'}
                placeholder={'Input Number A'}
                onChange={handleInputA}
            />

            <CalcButtons
                handleAdd={handleAdd}
                handleMinus={handleMinus}
                handleDivide={handleDivide}
                handleMultiply={handleMultiply}
            />

            <CalcInput
                label={'Number B'}
                placeholder={'Input Number B'}
                onChange={handleInputB}
            />

            <div className='d-flex justify-content-center'>
                <Button
                    className="btn-warning"
                    onClick={handleReset}
                >Reset</Button>
            </div>
            <h1 className="text-center">Result: {result}</h1>
        </>
    )
}

export default CalcApp;